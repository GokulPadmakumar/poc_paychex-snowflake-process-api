package utils;


import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.api.client.util.PemReader;
import com.google.api.client.util.PemReader.Section;
import com.google.api.client.util.SecurityUtils;

public class JWTToken {
	String str="";
	
public static String getJWTToken(String privateKeyPkcs8,String privateKeyId,String saEmail,String audience) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
	
		//System.out.println(privateKeyPkcs8);
		privateKeyPkcs8 = privateKeyPkcs8.replaceAll("\\\\n", "\n");
		//System.out.println(privateKeyPkcs8);
		
		Reader reader = new StringReader(privateKeyPkcs8);
		Section section = PemReader.readFirstSectionAndClose(reader);
		if (section == null) {
			throw new IOException("Invalid PKCS#8 data.");
		}
		byte[] bytes = section.getBase64DecodedBytes();
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
		KeyFactory keyFactory = SecurityUtils.getRsaKeyFactory();
		PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
		
		long now = System.currentTimeMillis();
		
		Algorithm algorithm = Algorithm.RSA256(null, (RSAPrivateKey) privateKey);
		String signedJwt = JWT.create()
				.withKeyId(privateKeyId)				
				.withIssuer(saEmail)				
				.withSubject(saEmail)
				.withAudience(audience)
				.withIssuedAt(new Date(now))
				.withExpiresAt(new Date(now + 3600 * 1000L))
				.sign(algorithm);
		
		return signedJwt;
	}
}
